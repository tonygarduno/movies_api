<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//Registro de usuarios tanto administradores como clientes
Route::post('register', 'Api\AuthController@register');

Route::group(['middleware'=>'auth:api'], function(){
    //Admin users
    Route::get('adminUsers', 'Api\UserController@getAdminUsers');
    Route::get('adminUsers/{id}', 'Api\UserController@getAdminUserDetails');
    Route::put('adminUsers', 'Api\UserController@updateAdminUser');
    Route::delete('adminUsers/{id}', 'Api\UserController@deleteAdminUser');
    //Customers
    Route::get('customerUsers', 'Api\CustomerController@getCustomerUsers');
    Route::get('customerUsers/{id}', 'Api\CustomerController@getCustomerDetails');
    Route::put('customerUsers', 'Api\CustomerController@updateCustomer');
    Route::delete('customerUsers/{id}', 'Api\CustomerController@deleteCustomer');
    //Movies
    Route::get('movies', 'Api\MovieController@getMovies');
    Route::get('movies/{id}', 'Api\MovieController@getMovieDetails');
    Route::post('movies', 'Api\MovieController@addMovie');
    Route::put('movies', 'Api\MovieController@updateMovie');
    Route::delete('movies/{id}', 'Api\MovieController@deleteMovie');
    //Comment
    Route::get('comments/{id}', 'Api\CommentController@getMovieComments');
    //Route::get('comments/{id}', 'Api\CommentController@getMovieCommentsDetails');
    Route::post('comments', 'Api\CommentController@addComment');
    Route::put('comments', 'Api\CommentController@updateComment');
    Route::delete('comments/{id}', 'Api\CommentController@deleteComment');
});
