<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserType extends Model
{
    const
        ADMIN = 1,
        CUSTOMER = 2;
}
