<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Movie extends Model
{
    protected $table = "movies";
    public $timestamps = false;

    //Relación One To Many
    public function comments(){
        return $this->hasMany('App\Comment');
    }


}
