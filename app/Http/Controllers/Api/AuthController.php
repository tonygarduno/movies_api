<?php
/**
 * Created by PhpStorm.
 * User: StarkIndustries
 * Date: 12/08/2018
 * Time: 07:28 PM
 */

namespace App\Http\Controllers\Api;


use App\Movie_fan;
use App\User;
use App\UserType;
use Illuminate\Http\Request;
use Validator;

class AuthController extends ApiController
{
    /**función de registro de nuevo usuario
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register (Request $request){
        //validación de datos requeridos
        $validator = Validator::make($request->all(),[
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required',
            'confirm_password' => 'required|same:password',
            'user_type' => 'required'
        ]);
        //Si existe error en los campos requeridos
        if($validator->fails()){
            return $this->sendError("Error de validación", $validator->errors(),422);
        }
        //Se valida que el user_type sea administrador (1) o sea customer(2)
        if($request->get('user_type') == UserType::ADMIN || $request->get('user_type') == UserType::CUSTOMER) {
            //creación del usuario
            $user = new User();
            $user->name = $request->get('name');
            $user->email = $request->get('email');
            //encriptamos la contraseña
            $user->password = bcrypt($request->get('password'));
            $user->user_type = $request->get('user_type');
            $user->save();
            //se crea un token
            $token = $user->createToken("Personal Access Token")->accessToken;
            //guardamos los datos en un array asociativo
            $data = [
                'token' => $token,
                'user' => $user
            ];
        }
        else{
            return $this->sendError("El tipo de usuario es incorrecto", [],422);
        }
        //regresamos una respuesta exitosa al registrar al usuario
        return $this->sendResponse($data,"Registro éxitoso");
    }
}