<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

/**Controlador para mensajes de exito y error personalizados
 * Class ApiController
 * @package App\Http\Controllers\Api
 */
class ApiController extends Controller
{
    /**función para mensajes de éxito con respuesta json
     * @param $result valores a regresar
     * @param $message mensaje a mostrar
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendResponse($result, $message){
        $response = [
            "success" => true,
            "data" => $result,
            "message" => $message
        ];
        return response()->json(
            $response,
            200
        );
    }

    /**Función para mensajes de error
     * @param $error error sucedido
     * @param array $errorMessages Mensajes de error
     * @param int $code codigo de error
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendError($error, $errorMessages = [], $code = 404){
        $response = [
            "success" => false,
            "error" => $error,
            "errorMessages" => $errorMessages
        ];
        return response()->json(
            $response,
            $code
        );
    }
}
