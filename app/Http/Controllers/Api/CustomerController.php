<?php

namespace App\Http\Controllers\Api;

use App\User;
use App\UserType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;
class CustomerController extends ApiController
{
    public function getCustomerUsers(){
        $data = [];
        //recuperamos todos los usuarios de la BD
        $customers = DB::table('users')
            ->where('user_type', '=', UserType::CUSTOMER)
            ->get(['id', 'name', 'email', 'user_type', 'created_at', 'updated_at']);

        //verificamos que no esté vacío
        if($customers->isEmpty()){
            return $this->sendError("No existen usuarios",[],422);
        }
        //guardamos los datos
        $data['customers'] = $customers;
        return $this->sendResponse($data, "Clientes recuperados exitosamente");
    }

    public function getCustomerDetails($id, Request $request){
        $customer = new User();
        $data = [];
        $result = $customer->find($id);
        //verificamos que no esté vacío
        if(!$result){
            return $this->sendError("No existe el cliente",[],422);

        }
        //guardamos los datos
        $data["customer"] = $result;
        return $this->sendResponse($data, "Datos de cliente recuperados correctamente");
    }

    /**Función para actualizar los datos de un usuario cliente
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateCustomer(Request $request)
    {
        //Buscamos al usuario por id y por user_type para validar que exista y corresponda a un usuario cliente
        $user = User::where('id', $request->get('id'))
            ->where('user_type', UserType::CUSTOMER)
            ->first();
        //En caso de que el usuario no exista mandamos un mensaje de error
        if (!$user) {
            return $this->sendError("No existe el usuario", [], 422);

        }
        //Realizamos un validator de los campos requeridos y que se permiten modificar
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email'
        ]);
        //Si la validación falla se envía un error
        if ($validator->fails()) {
            return $this->sendError("Error de validación", $validator->errors(), 422);
        }
        //Actualizamos los datos y guardamos con el método save
        $user->name = $request->get('name');
        $user->email = $request->get('email');
        $user->save();
        //Guardamos datos en array
        $data = [
            'user' => $user
        ];
        //regresamos respuesta
        return $this->sendResponse($data, "Usuario modificado con éxito");
    }

    /**Función para eliminar un usuario cliente
     * @param $id del usuario a eliminar
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteCustomer($id)
    {
        $user = User::where('id', $id)
            ->where('user_type', '=', UserType::CUSTOMER)
            ->first();

        if (!$user) {
            return $this->sendError("No existe el usuario", [], 422);
        }
        $user->delete();
        return $this->sendResponse('Ok', "Usuario eliminado con éxito");
    }
}
