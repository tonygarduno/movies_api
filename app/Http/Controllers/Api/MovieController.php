<?php

namespace App\Http\Controllers\Api;

use App\Movie;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;

class MovieController extends ApiController
{
    /**Función para obtener el listado de peliculas
     * @return \Illuminate\Http\JsonResponse
     */
    public function getMovies()
    {
        $data = [];
        //recuperamos todas las películas de la BD
        $movies = Movie::all();
        //verificamos que no esté vacío
        if ($movies->isEmpty()) {
            return $this->sendError("No existen películas", [], 422);
        }
        //guardamos las peliculas en un array
        $data['movies'] = $movies;
        //Retornamos respuesta
        return $this->sendResponse($data, "Películas recuperados exitosamente");
    }

    /**Función para mostrar los detalles de una pelicula
     * @param $id de la pelicula a buscar
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getMovieDetails($id, Request $request)
    {
        $movie = Movie::find($id);
        $data = [];

        if (!$movie) {
            return $this->sendError("No existe la pelicula", [], 422);

        }
        $data["movie"] = $movie;
        return $this->sendResponse($data, "Datos de película recuperados correctamente");
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addMovie(Request $request)
    {
        //validación de datos requeridos
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'synopsis' => 'required',
            'poster' => 'required',
            'review' => 'required',
            'releaseDate' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->sendError("Error de validación", $validator->errors(), 422);
        }

        //creación de la película
        $movie = new Movie();
        $movie->title = $request->get('title');
        $movie->synopsis = $request->get('synopsis');
        $movie->poster = $request->get('poster');
        $movie->review = $request->get('review');
        $movie->release_date = $request->get('releaseDate');
        $movie->save();

        return $this->sendResponse($movie, "Registro de película exitoso");
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateMovie(Request $request)
    {
        $movie = Movie::find($request->get('id'));

        if (!$movie) {
            return $this->sendError("No existe la película", [], 422);

        }

        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'synopsis' => 'required',
            'poster' => 'required',
            'review' => 'required',
            'releaseDate' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->sendError("Error de validación", $validator->errors(), 422);
        }

        $movie->title = $request->get('title');
        $movie->synopsis = $request->get('synopsis');
        $movie->poster = $request->get('poster');
        $movie->review = $request->get('review');
        $movie->release_date = $request->get('releaseDate');
        $movie->save();

        $data = [
            'movie' => $movie
        ];

        return $this->sendResponse($data, "Película modificada con éxito");
    }

    /**Función para eliminar una película
     * @param $id de la pelicula
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteMovie($id)
    {
        $movie = Movie::find($id);

        if (!$movie) {
            return $this->sendError("No existe la pelicula", [], 422);
        }
        $movie->delete();
        return $this->sendResponse('Ok', "Película eliminada con éxito");
    }
}
