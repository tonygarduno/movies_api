<?php

namespace App\Http\Controllers\Api;

use App\Comment;
use App\Movie;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;
class CommentController extends ApiController
{
    /**
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getMovieComments($id)
    {
        $movie = Movie::find($id);
        $data = [];

        if (!$movie) {
            return $this->sendError("No existe la pelicula", [], 422);

        }
        $comments = DB::table('comments')
            ->where('idmovie', '=', $id)
            ->get();

        $data["comments"] = $comments;
        return $this->sendResponse($data, "Comentarios de película recuperados correctamente");
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addComment(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'idMovieFan' => 'required',
            'idMovie' => 'required',
            'comment' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->sendError("Error de validación", $validator->errors(), 422);
        }
        $movieFan =  DB::table("users")
            ->where('id', '=', $request->get('idMovieFan'))
                ->where( 'user_type', '=', 2)
            ->get();

        if($movieFan->isEmpty()){
            return $this->sendError("El usuario no existe", $validator->errors(), 422);
        }

        $movie = new Movie();
        $result = $movie->find($request->get('idMovie'));
        if(!$result){
            return $this->sendError("La película no existe", [], 422);
        }

        $comment = new Comment();
        $comment->idmoviefan = $request->get('idMovieFan');
        $comment->idmovie = $request->get('idMovie');
        $comment->comment = $request->get('comment');
        $comment->save();

        return $this->sendResponse($comment,"Comentario guardado exitosamente");
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateMovie(Request $request)
    {
        $movie = Movie::find($request->get('id'));

        if (!$movie) {
            return $this->sendError("No existe la película", [], 422);

        }

        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'synopsis' => 'required',
            'poster' => 'required',
            'review' => 'required',
            'releaseDate' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->sendError("Error de validación", $validator->errors(), 422);
        }

        $movie->title = $request->get('title');
        $movie->synopsis = $request->get('synopsis');
        $movie->poster = $request->get('poster');
        $movie->review = $request->get('review');
        $movie->release_date = $request->get('releaseDate');
        $movie->save();

        $data = [
            'movie' => $movie
        ];

        return $this->sendResponse($data, "Película modificada con éxito");
    }

    /**Función para eliminar una película
     * @param $id de la pelicula
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteMovie($id)
    {
        $movie = Movie::find($id);

        if (!$movie) {
            return $this->sendError("No existe la pelicula", [], 422);
        }
        $movie->delete();
        return $this->sendResponse('Ok', "Película eliminada con éxito");
    }
}
