<?php

namespace App\Http\Controllers\Api;

use App\Movie_fan;
use App\User;
use App\UserType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;

class UserController extends ApiController
{
    /**Función para obtener todos los usuarios que son administradores del sistema user_type = 1
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAdminUsers()
    {
        $data = [];
        //recuperamos todos los usuarios de la BD que sean user_type = 1 y especificamos las columnas que deseamos obtener
        $users = DB::table('users')
            ->where('user_type', '=', UserType::ADMIN)
            ->get(['id', 'name', 'email', 'user_type', 'created_at', 'updated_at']);

        //verificamos que no esté vacío
        if ($users->isEmpty()) {
            return $this->sendError("No existen usuarios", [], 422);
        }
        //guardamos los datos
        $data['users'] = $users;
        //Se devuelve la respuesta
        return $this->sendResponse($data, "Usuarios recuperados exitosamente");
    }

    /**Función para obtener un usuario por id
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAdminUserDetails($id)
    {
        $data = [];
        //Buscamos al usuario por id para validar que exista
        $user = DB::table('users')
            ->where('id','=', $id)
            ->where('user_type', '=', UserType::ADMIN)
            ->get(['id', 'name', 'email', 'user_type', 'created_at', 'updated_at']);
        //En caso de que el usuario no exista mandamos un mensaje de error
        if (!$user) {
            return $this->sendError("No existe el usuario", [], 422);

        }
        //guardamos los datos
        $data["user"] = $user;
        //Se regresa la respuesta
        return $this->sendResponse($data, "Datos de usuario recuperados correctamente");
    }

    /**Función para actualizar los datos de un usuario administrador
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateAdminUser(Request $request)
    {
        //Buscamos al usuario por id y por user_type para validar que exista y corresponda a un usuario administrador
        $user = User::where('id', $request->get('id'))
            ->where('user_type', UserType::ADMIN)
            ->first();
        //En caso de que el usuario no exista mandamos un mensaje de error
        if (!$user) {
            return $this->sendError("No existe el usuario", [], 422);

        }
        //Realizamos un validator de los campos requeridos y que se permiten modificar
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email'
        ]);
        //Si la validación falla se envía un error
        if ($validator->fails()) {
            return $this->sendError("Error de validación", $validator->errors(), 422);
        }
        //Actualizamos los datos y guardamos con el método save
        $user->name = $request->get('name');
        $user->email = $request->get('email');
        $user->save();
        //Guardamos datos en array
        $data = [
            'user' => $user
        ];
        //regresamos respuesta
        return $this->sendResponse($data, "Usuario modificado con éxito");
    }

    /**Función para eliminar un usuario administrador
     * @param $id del usuario a eliminar
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteAdminUser($id)
    {
        $user = User::where('id', $id)
            ->where('user_type', UserType::ADMIN)
            ->get()
            ->first();

        if (!$user) {
            return $this->sendError("No existe el usuario", [], 422);
        }
        $user->delete();
        return $this->sendResponse('Ok', "Usuario eliminado con éxito");
    }
}
